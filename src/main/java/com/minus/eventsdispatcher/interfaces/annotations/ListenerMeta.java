package com.minus.eventsdispatcher.interfaces.annotations;

import java.lang.annotation.*;

/**
 * @author asafb
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface ListenerMeta {
    Event[] events() default {};


}
