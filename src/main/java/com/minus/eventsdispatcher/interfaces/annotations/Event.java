package com.minus.eventsdispatcher.interfaces.annotations;

import com.minus.eventsdispatcher.event.IEvent;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Created by asafb on 4/26/16.
 */


@Retention(RetentionPolicy.RUNTIME)
public @interface Event {
    Class<? extends IEvent> klazz();

    boolean enabled() default true;
}

