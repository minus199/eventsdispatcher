package com.minus.eventsdispatcher.interfaces;

import com.minus.eventsdispatcher.event.IEvent;

import java.util.Set;

/**
 * @author minus
 */
public interface IListenersRegistrar {

    IListenersRegistrar registerAll();

    boolean registerListener(Class<? extends IEvent> eventKlazz, Class<? extends IListener> iListener);

    Set<Class<? extends IListener>> unregisterEvent(Class<? extends IEvent> eventKlazz);

    Set<Class<? extends IListener>> getListeners(Class<? extends IEvent> eventClass);

    Set<Class<? extends IEvent>> getEvents();

}
