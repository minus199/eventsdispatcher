package com.minus.eventsdispatcher.interfaces;

import com.minus.eventsdispatcher.event.IEvent;

/**
 *
 * @author asafb
 */

@FunctionalInterface
public interface IListener {
    void execute(IEvent event);
}
