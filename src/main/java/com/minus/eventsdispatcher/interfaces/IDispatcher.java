package com.minus.eventsdispatcher.interfaces;


import com.minus.eventsdispatcher.event.IEvent;

import java.util.Set;

/**
 * @author asafb
 */
public interface IDispatcher {

    IDispatcher dispatch(IEvent event);

    IDispatcher dispatchSynchronous(IEvent event);

    IDispatcher registerListener(Class<? extends IEvent> iEvent, Class<? extends IListener> iListener);

    /**
     * @param iEvent
     * @return List of <code>Class<? extends IListener></code> which represent the listeners which were registered
     * to the passed event and were dropped.
     */
    Set<Class<? extends IListener>> unregisterEvent(Class<? extends IEvent> iEvent);


}
