package com.minus.eventsdispatcher;

import com.minus.eventsdispatcher.event.IEvent;
import com.minus.eventsdispatcher.interfaces.IDispatcher;
import com.minus.eventsdispatcher.interfaces.IListener;
import com.minus.eventsdispatcher.interfaces.IListenersRegistrar;

import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;


/**
 * @author minus
 */
final public class Dispatcher implements IDispatcher {

    private static final org.apache.logging.log4j.Logger logger = org.apache.logging.log4j.LogManager.getLogger(Dispatcher.class.getName());

    final private IListenersRegistrar eventsRegistrar = new MainListenersRegistrar().registerAll();
    final private ExecutorService executorService = Executors.newCachedThreadPool();

    private Dispatcher() {
        registerShutdown();
    }

    private static class InstanceContainer {
        private static final IDispatcher instance = new Dispatcher();
    }

    public static IDispatcher manage() {
        return InstanceContainer.instance;
    }

    @Override
    public IDispatcher dispatchSynchronous(IEvent event) {
        new DispatcherRunnable(event, eventsRegistrar.getListeners(event.getClass())).run();
        return this;
    }

    @Override
    final public IDispatcher dispatch(IEvent event) {
        if (eventsRegistrar.getEvents().contains(event.getClass())) {
            executorService.submit(new DispatcherRunnable(event, eventsRegistrar.getListeners(event.getClass())));
        }

        return this;
    }


    @Override
    public IDispatcher registerListener(Class<? extends IEvent> iEvent, Class<? extends IListener> iListener) {
        eventsRegistrar.registerListener(iEvent, iListener);
        return this;
    }

    @Override
    public Set<Class<? extends IListener>> unregisterEvent(Class<? extends IEvent> iEvent) {
        return eventsRegistrar.unregisterEvent(iEvent);
    }

    private void registerShutdown() {
        Runtime.getRuntime().addShutdownHook(new Thread(() -> {
            executorService.shutdown();
            try {
                executorService.awaitTermination(Long.MAX_VALUE, TimeUnit.DAYS);
            } catch (InterruptedException ignore) {

            }
        }));
    }
}
