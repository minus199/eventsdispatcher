package com.minus.eventsdispatcher.containers;

/**
 * Created by minus on 4/28/16.
 */
public class GenericKeyValuePair <K, V> {
    private final K key;
    private final V value;

    public GenericKeyValuePair(K key, V value) {
        this.key = key;
        this.value = value;
    }

    public K getKey() {
        return key;
    }

    public V getValue() {
        return value;
    }
}