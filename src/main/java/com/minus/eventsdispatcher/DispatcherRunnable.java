package com.minus.eventsdispatcher;

import com.minus.eventsdispatcher.event.IEvent;
import com.minus.eventsdispatcher.interfaces.IListener;
import org.apache.logging.log4j.Logger;

import java.lang.reflect.InvocationTargetException;
import java.util.Set;

/**
 * @author minus
 */
class DispatcherRunnable implements Runnable {

    private static final Logger logger = org.apache.logging.log4j.LogManager.getLogger(DispatcherRunnable.class.getName());

    private final IEvent event;
    private final Set<Class<? extends IListener>> listeners;

    DispatcherRunnable(IEvent event, Set<Class<? extends IListener>> listeners) {
        this.event = event;
        this.listeners = listeners;
    }

    @Override
    public void run() {
        for (Class<? extends IListener> iListener : listeners) {
            logger.trace("Dispatching " + event.getClass().getName() + " with " + iListener.getClass().getSimpleName());
            try {
                iListener.getConstructor(IEvent.class).newInstance(event);
            } catch (InstantiationException | IllegalAccessException | NoSuchMethodException | InvocationTargetException e) {
                logger.error("Error during dispatching. Make sure listener has a constructor with event.", e);
            }
        }
    }
}
