package com.minus.eventsdispatcher.event;

import com.google.gson.Gson;
import com.minus.eventsdispatcher.containers.GenericKeyValuePair;

import java.util.HashMap;
import java.util.Map;

/**
 * @author minus
 */
public interface IEvent {
    /**
     * Convert this object to map
     *
     * @return
     */
    Map toMap();

    /**
     * The data contained by the Event object will be passed along to the command string
     *
     * @param metaArgs -- any additional args not contained in the event for some reason (pass null or empty if none)
     * @return
     */
    default String buildCommand(GenericKeyValuePair<String, Object>... metaArgs) {
        HashMap<Object, Object> data = new HashMap<>();

        data.put("command", getType());
        data.put("params", toMap());

        if (metaArgs != null && metaArgs.length > 0){
            data.put("meta", metaArgs);
        }

        return new Gson().toJson(data);
    }

    default String getType() {
        return getClass().getName();
    }
}
