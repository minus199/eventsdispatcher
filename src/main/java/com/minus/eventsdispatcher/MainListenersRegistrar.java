package com.minus.eventsdispatcher;

import com.minus.eventsdispatcher.interfaces.annotations.Event;
import com.minus.eventsdispatcher.interfaces.annotations.ListenerMeta;
import com.minus.eventsdispatcher.event.IEvent;
import com.minus.eventsdispatcher.interfaces.IListener;
import com.minus.eventsdispatcher.interfaces.IListenersRegistrar;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.Logger;
import org.reflections.Reflections;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * @author MiNuS420 <minus199@gmail.com>
 */
class MainListenersRegistrar implements IListenersRegistrar {

    private static final Logger logger = org.apache.logging.log4j.LogManager.getLogger(MainListenersRegistrar.class.getName());
    private final HashMap<Class<? extends IEvent>, Set<Class<? extends IListener>>> listeners = new HashMap<Class<? extends IEvent>, Set<Class<? extends IListener>>>();

    @Override
    /**
     * Scan for all listeners and registerAll according to metadata
     */
    public IListenersRegistrar registerAll() {
        findListeners().forEach(listener -> {
            ListenerMeta listenerMeta = listener.getAnnotation(ListenerMeta.class);

            Stream.of(listenerMeta.events())
                    .filter(Event::enabled)
                    .forEach(event -> {
                        try {
                            registerListener(event.klazz(), listener);
                        } catch (Exception e) {
                            logger.log(Level.WARN, "unable to invoke " + String.valueOf(listener), e);
                        }
                    });
        });

        return this;
    }

    @Override
    public boolean registerListener(Class<? extends IEvent> eventKlazz, Class<? extends IListener> iListener) {
        listeners.putIfAbsent(eventKlazz, new HashSet<Class<? extends IListener>>());
        return listeners.get(eventKlazz).add(iListener);
    }

    @SuppressWarnings("unchecked")
    private Set<Class<? extends IListener>> findListeners() {
        return new Reflections(this).getTypesAnnotatedWith(ListenerMeta.class).stream()
                .map((Function<Class<?>, Class<? extends IListener>>) aClass -> aClass.asSubclass(IListener.class))
                .collect(Collectors.toSet());
    }

    @Override
    public Set<Class<? extends IListener>> unregisterEvent(Class<? extends IEvent> eventKlazz) {
        return listeners.remove(eventKlazz);
    }

    @Override
    public Set<Class<? extends IListener>> getListeners(Class<? extends IEvent> eventClass) {
        return listeners.get(eventClass);
    }

    @Override
    public Set<Class<? extends IEvent>> getEvents() {
        return listeners.keySet();
    }

}
